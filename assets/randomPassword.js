let symbols = null;
let digits = null;
let lowercaseLetters = null;
let uppercaseLetters = null;
const minLength = 15;
const maxLength = 94;

const Type = Object.freeze({
    UPPERCASE_LETTER: 0,
    LOWERCASE_LETTER: 1,
    DIGIT: 2,
    SYMBOL: 3
});

const pwd = {
    stringInit() {
        symbols = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~';
        digits = '0123456789';
        lowercaseLetters = 'abcdefghijklmnopqrstuvwxyz';
        uppercaseLetters = lowercaseLetters.toUpperCase();
    },

    myRand(a, b) {
        const min = Math.min(a, b);
        const max = Math.max(a, b);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    // Checks if value is between a and b
    isBetween(value, a, b) {
        const min = Math.min(a, b);
        const max = Math.max(a, b);
        return min <= value && value <= max;
    },

    getType(character) {
        const asciiCode = character.charCodeAt(0);
        let t = null;

        if (pwd.isBetween(asciiCode, 48, 57)) {
            t = Type.DIGIT;
        } else if (pwd.isBetween(asciiCode, 65, 90)) {
            t = Type.UPPERCASE_LETTER;
        } else if (pwd.isBetween(asciiCode, 97, 122)) {
            t = Type.LOWERCASE_LETTER;
        } else {
            t = Type.SYMBOL;
        }

        return t;
    },

    // In order to prevent two consecutive characters of the same type
    getRandomType(excludedType, alphanumeric) {
        const types = [];

        if (digits && excludedType !== Type.DIGIT) {
            types.push(Type.DIGIT);
        }

        if (uppercaseLetters && excludedType !== Type.UPPERCASE_LETTER) {
            types.push(Type.UPPERCASE_LETTER);
        }

        if (lowercaseLetters && excludedType !== Type.LOWERCASE_LETTER) {
            types.push(Type.LOWERCASE_LETTER);
        }

        if (!alphanumeric && symbols && excludedType !== Type.SYMBOL) {
            types.push(Type.SYMBOL);
        }

        const randomIndex = pwd.myRand(0, types.length - 1);
        return types[randomIndex];
    },

    getRandomChar(type, uniqueCharacters) {
        let str = '';

        switch (type) {
            case Type.UPPERCASE_LETTER:
                str = uppercaseLetters;
                break;
            case Type.LOWERCASE_LETTER:
                str = lowercaseLetters;
                break;
            case Type.DIGIT:
                str = digits;
                break;
            default:
                str = symbols;
                break;
        }

        const randomIndex = pwd.myRand(0, str.length - 1);
        const randomChar = str.charAt(randomIndex);

        if (uniqueCharacters) {
            switch (type) {
                case Type.UPPERCASE_LETTER:
                    uppercaseLetters = uppercaseLetters.replace(randomChar, '');
                    break;
                case Type.LOWERCASE_LETTER:
                    lowercaseLetters = lowercaseLetters.replace(randomChar, '');
                    break;
                case Type.DIGIT:
                    digits = digits.replace(randomChar, '');
                    break;
                default:
                    symbols = symbols.replace(randomChar, '');
                    break;
            }
        }

        return randomChar;
    },

    buildPassword(length, alphanumeric, uniqueCharacters) {
        pwd.stringInit();

        let password = '';

        let randomType = pwd.getRandomType(null, alphanumeric);
        let randomChar = pwd.getRandomChar(randomType, uniqueCharacters);
        password += randomChar;

        let previousChar, previousCharType;

        if (alphanumeric && uniqueCharacters && length > 62) {
            length = 62;
        } else if (length > maxLength) {
            uniqueCharacters = false;
        } else if (length < minLength) {
            length = minLength;
        }

        while (password.length < length) {
            previousChar = password.charAt(password.length - 1);
            previousCharType = pwd.getType(previousChar);

            randomType = pwd.getRandomType(previousCharType, alphanumeric);
            randomChar = pwd.getRandomChar(randomType, uniqueCharacters);

            password += randomChar;
        }

        return password;
    },

    copyToClipboard() {
        const copyText = document.querySelector('#passwords');
        copyText.select();
        document.execCommand('copy');
    },

    clear() {
        document.querySelector('#passwords').value = '';
    }
};

// eslint-disable-next-line prettier/prettier
export default pwd;