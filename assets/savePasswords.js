const FileSaver = require('file-saver');
const savePD = {
    newFilename() {
        const d = new Date();
        const year = d.getFullYear();
        const month = d.getMonth() + 1;
        const day = d.getDate();
        const hour = d.getHours();
        const minutes = d.getMinutes();
        const seconds = d.getSeconds();

        const time = ''.concat(
            year,
            '-',
            month,
            '-',
            day,
            '_',
            hour,
            '_',
            minutes,
            '_',
            seconds
        );

        return ''.concat('passwords-', time, '.txt');
    },
    downloadPasswords() {
        const filename = savePD.newFilename();
        const passwordList = document.querySelector('#passwords').value;
        const file = new File([passwordList], filename, {
            type: 'text/plain;charset=utf-8'
        });
        FileSaver.saveAs(file);
    }
};

// eslint-disable-next-line prettier/prettier
export default savePD;