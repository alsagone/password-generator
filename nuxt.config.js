export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: 'Random password generator',
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: process.env.npm_package_description || ''
            }
        ],
        link: [{ rel: 'icon', type: 'image/x-icon', href: 'favicon.ico?v2' }]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#fff' },

    /*
     ** Customize the generated output folder
     */
    generate: {
        dir: 'public'
    },

    /*
     ** Customize the base url
     */
    router: {
        base: '/password-generator/'
    },

    /*
     ** Global CSS
     */
    css: ['@/assets/scss/style.css'],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [
        // Doc: https://github.com/nuxt-community/eslint-module
        '@nuxtjs/eslint-module',
        // Doc: https://github.com/nuxt-community/stylelint-module
        '@nuxtjs/stylelint-module',
        '@nuxtjs/vuetify'
    ],
    /*
     ** Nuxt.js modules
     */
    modules: [
        // Doc: https://bootstrap-vue.js.org
        'bootstrap-vue/nuxt'
    ],
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {}
    }
};